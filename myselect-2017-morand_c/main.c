/*
** main.c for main for test dll in /home/morand_c//SVN/myselect-2017-morand_c
**
** Made by raphael morand
** Login   <morand_c@epitech.net>
**
** Started on  Wed Jan 16 14:17:57 2013 raphael morand
** Last update Fri Jan 18 18:09:23 2013 raphael morand
*/

#include "dll.h"
#include "get_key.h"
#include "my_select.h"

void my_select(t_dlist *d_list) {
  int c;
  int i;

  i = 0;
  while (i == 0) {
    c = 0;
    if (d_list->size != 0) {
      d_list = get_key(c, d_list);
      view(d_list);
      my_printf("elem number = %d, pos = %d\n", d_list->size, d_list->pos);
    } else
      i = 1;
  }
}

int usage(int n) {
  if (n == 0) {
    my_printf("USAGE : ./my_select ARGV[n] ARGV[n+1] ....\n");
    my_printf("\tuse --help to display full usage\n");
  } else if (n == 1) {
    my_printf("USAGE :\n");
    my_printf("\t./my_select ARGV[n] ARGV[n+1]...:\n");
    my_printf("\tto navigate use up or down arrows\n");
    my_printf("\tto select use spacebar\n");
    my_printf("\tto to delete use del or backspace\n");
    my_printf("\tto validate selection press enter\n");
    my_printf("\tto quit use ESC\n");
  }
  return (EXIT_FAILURE);
}

int main(int ac, char **av) {
  int n;
  t_dlist d_list;

  d_list.pos = 1;
  if (ac == 1)
    return usage(0);
  else if (ac == 2 && (my_strcmp(av[1], "--help")) == 0)
    return usage(1);
  else {
    init(&d_list);
    n = 1;
    set_input_mode();
    while (n < ac)
      pushback(&d_list, av[n++]);
    circularize(&d_list);
    view(&d_list);
    my_select(&d_list);
  }
}
