/*
** view_list.c for view in /home/morand_c//my_select
**
** Made by raphael morand
** Login   <morand_c@epitech.net>
**
** Started on  Wed Jan 16 10:26:37 2013 raphael morand
** Last update Fri Jan 18 15:37:57 2013 raphael morand
*/

#include "LIB/my.h"
#include "dll.h"

void view(t_dlist *l) {
  t_elem *elem;

  l->last->next = NULL;
  if (tgetent(NULL, "xterm") <= 0)
    exit(EXIT_FAILURE);
  elem = l->first;
  my_printf("%s%s", tgetstr("cl", NULL), tgoto(tgetstr("cm", NULL), 0, 0));
  while (elem != NULL) {
    if (elem->point == 1 && elem->select == 1)
      my_printf("%s%s%s%s\n", tgetstr("us", NULL), tgetstr("mr", NULL),
                (char *)elem->data, tgetstr("me", NULL));
    else if (elem->point == 1 && elem->select == 0)
      my_printf("%s%s%s\n", tgetstr("us", NULL), (char *)elem->data,
                tgetstr("me", NULL));
    else if (elem->select == 1 && elem->point == 0)
      my_printf("%s%s%s\n", tgetstr("mr", NULL), (char *)elem->data,
                tgetstr("me", NULL));
    else
      my_printf("%s\n", (char *)elem->data);
    elem = elem->next;
  }
  l->last->next = l->first;
}
