/*
** get_key.h for get_key in /home/morand_c//SVN/myselect-2017-morand_c
**
** Made by raphael morand
** Login   <morand_c@epitech.net>
**
** Started on  Thu Jan 17 11:38:13 2013 raphael morand
** Last update Fri Jan 18 13:54:51 2013 raphael morand
*/

#ifndef GET_KEY_
#define GET_KEY_

void droite(char c);
void gauche(char c);
void bas(char c);
void haut(char c);
int detect_arrows(char c);
t_dlist *xselect(t_dlist *d_list);
int delete (t_dlist *d_list);
t_dlist *xsuppr(t_dlist *d_list);
void set_input_mode(void);

#endif
