/*
** my_double_linked_list_create.c for dll in /home/morand_c//my_select
**
** Made by raphael morand
** Login   <morand_c@epitech.net>
**
** Started on  Wed Jan 16 10:12:56 2013 raphael morand
** Last update Fri Jan 18 18:11:52 2013 raphael morand
*/

#include "dll.h"
#include "my_select.h"

void init(t_dlist *l) {
  l->first = NULL;
  l->last = NULL;
  l->size = 0;
}

void pushback(t_dlist *l, void *val) {
  t_elem *nouv;

  if ((nouv = malloc(sizeof(t_elem))) == NULL)
    exit(EXIT_FAILURE);
  l->size++;
  nouv->data = val;
  nouv->next = NULL;
  nouv->prev = l->last;
  if (l->size == 1)
    nouv->point = 1;
  else
    nouv->point = 0;
  if (l->last)
    l->last->next = nouv;
  else
    l->first = nouv;
  l->last = nouv;
}

void circularize(t_dlist *dlist) {
  t_elem *elem;

  dlist->first->prev = dlist->last;
  dlist->last->next = dlist->first;
}
