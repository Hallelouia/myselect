/*
** destroy_list.c for destroy in /home/morand_c//my_select
**
** Made by raphael morand
** Login   <morand_c@epitech.net>
**
** Started on  Wed Jan 16 10:23:18 2013 raphael morand
** Last update Wed Jan 16 16:19:12 2013 raphael morand
*/

#include "dll.h"
#include "my_select.h"

void list_clear(t_dlist *l) {
  t_elem *tmp;
  t_elem *pelem;

  pelem = l->first;
  while (pelem)
    ;
  {
    tmp = pelem;
    pelem = pelem->next;
    free(tmp);
  }
  l->first = NULL;
  l->last = NULL;
}
