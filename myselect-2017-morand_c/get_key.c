/*
** test.c for test in /home/morand_c//SVN/myselect-2017-morand_c
**
** Made by raphael morand
** Login   <morand_c@epitech.net>
**
** Started on  Thu Jan 17 10:21:42 2013 raphael morand
** Last update Fri Jan 18 18:15:19 2013 raphael morand
*/

#include "dll.h"
#include "my_select.h"
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>

void set_input_mode(void) {
  struct termios tattr;

  if ((tcgetattr(STDIN_FILENO, &tattr)) == -1)
    exit(EXIT_FAILURE);
  tattr.c_lflag &= ~(ICANON | ECHO);
  tattr.c_cc[VMIN] = 1;
  tattr.c_cc[VTIME] = 0;
  if ((tcsetattr(STDIN_FILENO, TCSAFLUSH, &tattr)) == -1)
    exit(EXIT_FAILURE);
}

t_dlist *xnext(t_dlist *d_list) {
  go_prev(d_list);
  (d_list->pos)--;
  if (d_list->pos <= 0)
    d_list->pos = d_list->size;
  return (d_list);
}

t_dlist *xprev(t_dlist *d_list) {
  go_next(d_list);
  (d_list->pos)++;
  if (d_list->pos > d_list->size)
    d_list->pos = 1;
  return (d_list);
}

t_dlist *xsuppr(t_dlist *d_list) {
  go_next(d_list);
  dlist_rm_elemn(d_list);
  if (d_list->pos >= d_list->size)
    d_list->pos = 1;
  d_list->size -= 1;
  return (d_list);
}

t_dlist *get_key(int c, t_dlist *d_list) {
  int j;

  j = 0;
  if ((j = read(0, &c, sizeof(int))) <= 0)
    return (d_list);
  if (c == TOUCH_UP)
    d_list = xnext(d_list);
  else if (c == TOUCH_DOWN)
    d_list = xprev(d_list);
  else if (c == TOUCH_DEL)
    d_list = xsuppr(d_list);
  else if (c == TOUCH_ENTER)
    validate(d_list);
  else if (c == TOUCH_SPACE)
    d_list = xselect(d_list);
  else if (c == TOUCH_BCKSPACE)
    d_list = xsuppr(d_list);
  else if (c == TOUCH_ESC)
    exit(EXIT_SUCCESS);
  return (d_list);
}
