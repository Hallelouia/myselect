/*
** my_strlowcase.c for my_strlowcase in /home/morand_c//piscine/Jour_06/ex_08
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Tue Oct  9 16:08:23 2012 raphael morand
** Last update Wed Oct 10 17:04:22 2012 raphael morand
*/

char	*my_strlowercase(char *str)
{
  int	n;

  n = 0;
  while (str[n] != '\0')
    {
      if (str[n] >= 65 && str[n] <= 90)
	str[n] += 32;
      n += 1;
    }
  return (str);
}
