/*
** my_printf.c for my_printf in /home/hallelouia/projets/my_printf
**
** Made by raphael morand
** Login   <morand_c@epitech.eu>
**
** Started on  Tue Nov 13 16:39:47 2012 raphael morand
** Last update Sat Nov 17 17:00:56 2012 raphael morand
*/

#include "my.h"
#include "my_printf.h"
#include <stdarg.h>
#include <stdlib.h>

void tabfct(int (*tabfunc[13])(va_list)) {
  tabfunc[0] = &list_putstr;
  tabfunc[1] = &list_putnbr;
  tabfunc[2] = &list_putnbr;
  tabfunc[3] = &list_usputnbr;
  tabfunc[4] = &list_octal;
  tabfunc[5] = &list_hexamin;
  tabfunc[6] = &list_hexamaj;
  tabfunc[7] = &list_bin;
  tabfunc[8] = &list_pourcent;
  tabfunc[9] = &list_putstr_inv;
  tabfunc[10] = &list_putchar;
  tabfunc[11] = &list_ptadress;
  tabfunc[12] = &list_error;
}

void tabindf(char tabind[13]) {
  tabind[0] = 's';
  tabind[1] = 'i';
  tabind[2] = 'd';
  tabind[3] = 'u';
  tabind[4] = 'o';
  tabind[5] = 'x';
  tabind[6] = 'X';
  tabind[7] = 'b';
  tabind[8] = '%';
  tabind[9] = 'S';
  tabind[10] = 'c';
  tabind[11] = 'p';
  tabind[12] = '\0';
}

int aff(int a, const char *format, va_list list) {
  int n;
  int (*tabfunc[13])(va_list);
  char tabind[13];

  n = 0;
  tabfct(tabfunc);
  tabindf(tabind);
  while (format[a] != tabind[n] && tabind[n] != 0)
    n++;
  if (tabind[n] == 0) {
    tabfunc[12](list);
    my_putchar(format[a]);
    a++;
    return (a);
  }
  tabfunc[n](list);
  a++;
  return (a);
}

int my_printf(const char *format, ...) {
  va_list ap;
  int a;

  va_start(ap, format);
  a = 0;
  while (format[a] != '\0') {
    if (format[a] == '%') {
      a++;
      a = aff(a, format, ap);
    } else
      my_putchar(format[a++]);
    va_end(ap);
  }
  return 0;
}
