/*
** my_putstr.c for my_putstr in /home/morand_c//afs/racmorand_c/rendu/piscine/Jour_04
**
** Made by raphael morand
** Login   <morand_c@epitech.net>
**
** Started on  Thu Oct  4 18:47:02 2012 raphael morand
** Last update Wed Oct 10 10:41:04 2012 raphael morand
*/

#include "my.h"

int     my_putstr(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      my_putchar(str[i]);
      i += 1;
    }
    return i;
}
