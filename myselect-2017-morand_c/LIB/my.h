/*
** my.h for my in /home/morand_c//afs/rendu
**
** Made by raphael morand
** Login   <morand_c@epitech.net>
**
** Started on  Fri Oct 12 13:07:30 2012 raphael morand
// Last update Thu Jan 10 12:52:44 2013 raphael morand
*/

#ifndef _MY_H
#define _MY_H

#include "my_printf.h"
#include <unistd.h>

int my_getnbr(char *str);
char *pathcut(char **env);
char **my_str_to_word_tab(char *str, char c);
char **my_str_to_word_tab2(char *str, char c);
int my_strlen(char *str);
void my_putchar(char c);
int my_isneg(int nb);
int my_put_nbr(int nb);
int my_swap(int *a, int *b);
int my_putstr(char *str);
void my_sort_int_tab(int *, int size);
int my_power_rec(int nb, int power);
int my_square_root(int nb);
int my_is_prime(int nombre);
int my_find_prime_sup(int nb);
char *my_strcpy(char *dest, char *src);
char *my_strncpy(char *dest, char *src, int nb);
char *my_revstr(char *str);
char *my_strstr(char *str, char *to_find);
int my_strcmp(char *s1, char *s2);
int my_strncmp(char *s1, char *s2, int nb);
char *my_strupcase(char *str);
char *my_strlowcase(char *str);
char *my_strcapitalize(char *str);
int my_str_isalpha(char *str);
int my_str_isnum(char *str);
int my_str_islower(char *str);
int my_str_isupper(char *str);
int my_str_isprintable(char *str);
int my_showstr(char *str);
int my_showmem(char *str, int size);
char *my_strcat(char *dest, char *src);
char *my_strncat(char *dest, char *src, int nb);
int my_strlcat(char *dest, char *src, int size);
void putstr_na(char *str);
void my_putnbr_base(int nb, char *base);
void us_putnbr_base_min(unsigned int nb, char *base);
void us_putnbr_base(unsigned int nb, char *base);
int my_printf(const char *format, ...);
#endif
