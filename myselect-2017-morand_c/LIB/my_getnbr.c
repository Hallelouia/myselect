int my_getnbr(char *str) {
  char signe;
  int nb;
  int n;

  n = 0;
  signe = 1;
  nb = 0;
  while (str[n] == '-' || *str == '+') {
    signe = str[n] == '-' ? signe *= -1 : signe;
    n += 1;
  }
  while (str[n] >= '0' && str[n] <= '9') {
    nb = (nb * 10 + *str - 48);
    if (nb > 0)
      nb *= -1;
    n += 1;
  }
  if (signe > 0)
    (nb = nb * -1);
  if (nb > 214743647 || nb < -214743648)
    return (0);
  return (nb);
}
