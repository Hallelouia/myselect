/*
** list_functions3.c for list_functions3 in /home/hallelouia/projets/my_printf
**
** Made by raphael morand
** Login   <morand_c@epitech.eu>
**
** Started on  Thu Nov 15 09:02:54 2012 raphael morand
** Last update Fri Nov 16 16:53:01 2012 raphael morand
*/

#include "my.h"
#include <stdarg.h>

int list_putchar(va_list list) {
  int a;
  char c;

  a = va_arg(list, int);
  c = (char)a;
  my_putchar(c);
  return 0;
}

int list_error(va_list list) {
  va_arg(list, int *);
  my_putchar('%');
  return (-1);
}
