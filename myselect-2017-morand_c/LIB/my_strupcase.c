/*
** my_strupcase.c for my_strupcase in /home/morand_c//piscine/Jour_06/ex_07
**
** Made by raphael morand
** Login   <morand_c@epitech.net>
**
** Started on  Tue Oct  9 15:19:42 2012 raphael morand
** Last update Tue Oct  9 16:07:50 2012 raphael morand
*/

char *my_strupcase(char *str) {
  int n;

  n = 0;
  while (str[n] != '\0') {
    if (str[n] >= 97 && str[n] <= 122)
      str[n] -= 32;
    n += 1;
  }
  return (str);
}
