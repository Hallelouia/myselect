/*
** my_str_isprintable.c for my_str_isprintable in /home/morand_c//piscine/Jour_06/ex_14
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Tue Oct  9 21:06:26 2012 raphael morand
** Last update Tue Oct  9 21:15:19 2012 raphael morand
*/

int	my_str_isprintable(char *str)
{
  int	n;

  n = 0;
  while (str[n] > '\0')
    {
      if (str[n] < 32 || str[n] == 127)
	return (0);
      n += 1;
    }
  return (1);
}
