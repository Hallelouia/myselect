/*
** my_strcpy.c for my_strcpy in /home/morand_c//afs/rendu/piscine/Jour_06
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Mon Oct  8 09:35:53 2012 raphael morand
** Last update Sat Nov 17 17:08:39 2012 raphael morand
*/

char	*my_strcpy(char *dest, char *src)
{
  int	i;

  i = 0;
  while (src[i] != '\0')
    {
      dest[i] = src[i];
      i += 1;
    }
  if (src[i] == '\0')
    dest[i] = '\0';
  return (dest);
}
