/*
** putnbropti.c for putnbropti in /home/hallelouia/lib
** 
** Made by raphael morand
** Login   <morand_c@epitech.eu>
** 
** Started on  Wed Nov 14 09:30:33 2012 raphael morand
** Last update Fri Nov 16 14:30:58 2012 raphael morand
*/

#include "my.h"
#include "my_printf.h"

void	my_putnbr_base(int nb, char *base)
{
  int idbase;
  int coef;

  coef = 1;
  idbase = my_strlen(base);
  if (nb < 0)
    {
      my_putchar('-');
      coef *= -1;
    }
  if (nb > idbase - 1 || nb < 0)
    {
      my_putnbr_base((nb / idbase) * coef, base);
      my_putnbr_base((nb % idbase) * coef, base);
    }
  else
    my_putchar(base[nb]);
}

void	us_putnbr_base_min(unsigned int nb, char *base)
{
  int	idbase;

  idbase = my_strlen(base);
  if (nb > idbase - 1)
    {
      us_putnbr_base_min(nb / idbase, base);
      us_putnbr_base_min(nb % idbase, base);
    }
  else
    my_putchar(base[nb]);
}

void	us_putnbr_base(unsigned int nb, char *base)
{
  int	id_base;

  id_base = my_strlen(base);
  if (nb > id_base - 1)
    {
      us_putnbr_base(nb / id_base, base);
      us_putnbr_base(nb % id_base, base);
    }
  else
    my_putchar(base[nb]);
}
