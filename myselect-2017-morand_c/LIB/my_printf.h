/*
** my_printf.h for my_printf in /home/hallelouia/SVN/my_printf-2017-morand_c
**
** Made by raphael morand
** Login   <morand_c@epitech.net>
**
** Started on  Sat Nov 17 17:01:42 2012 raphael morand
// Last update Sat Nov 17 17:02:55 2012 raphael morand
*/

#ifndef MY_PRINTF_H_
#define MY_PRINTF_H_
#include <stdarg.h>

int list_putstr(va_list list);
int list_putnbr(va_list list);
int y_strlen(char *str);
int list_usputnbr(va_list list);
int list_octal(va_list list);
int list_hexamin(va_list list);
int list_hexamaj(va_list list);
int list_bin(va_list list);
int list_pourcent(va_list list);
int list_putstr_inv(va_list list);
int list_putchar(va_list list);
int list_ptadress(va_list list);
int list_error(va_list list);

#endif /* !MY_PRINTF_H_ */
