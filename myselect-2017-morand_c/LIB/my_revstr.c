#include "my.h"

char *my_revstr(char *str) {
  int count;
  int i;
  char tmp;

  i = 0;
  count = my_strlen(str);
  count -= 1;
  while (i <= count / 2) {
    tmp = str[i];
    str[i] = str[count];
    str[count] = tmp;
    i += 1;
    count -= 1;
  }
  return (str);
}
