/*
** my_str_islower.c for my_str_islower in /home/morand_c//piscine/Jour_06/ex_12
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Tue Oct  9 20:05:06 2012 raphael morand
** Last update Wed Oct 10 09:29:59 2012 raphael morand
*/

int	my_str_islower(char *str)
{
  int	n;

  n = 0;
  while (str[n] != '\0')
    {
    if (str[n] < 97 || str[n] > 122)
      return (0);
  n += 1;
    }
  return (1);
}
