/*
** testnb.c for test in /home/morand_c/
**
** Made by raphael morand
** Login   <morand_c@epitech.net>
**
** Started on  Thu Oct  4 12:02:28 2012 raphael morand
** Last update Sat Nov 17 17:06:57 2012 raphael morand
*/

#include "my.h"

int my_putnbr(int val) {
  int coef;

  coef = 1;
  if (val < 0) {
    write(1, "-", 1);
    coef = -1;
  }
  if (val > 9 || val < 0)
    my_putnbr((val / 10) * coef);
  val = ((val % 10) * coef) + 48;
  write(1, &val, 1);
  return val;
}
