/*
** my_strlen.c for my_strlen in /home/hallelouia/lib/my
**
** Made by raphael morand
** Login   <morand_c@epitech.eu>
**
** Started on  Fri Nov 16 08:48:48 2012 raphael morand
** Last update Fri Nov 16 08:50:28 2012 raphael morand
*/

#include "my.h"

int my_strlen(char *str) {
  int n;

  n = -1;
  while (str[++n] != 0)
    ;
  return (n);
}
