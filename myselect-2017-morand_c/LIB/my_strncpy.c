/*
** my_strncpy.c for my_strncpy in /home/morand_c//afs/rendu/piscine/Jour_06
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Mon Oct  8 10:10:47 2012 raphael morand
** Last update Sat Nov 17 17:09:05 2012 raphael morand
*/

char	*my_strncpy(char *dest, char *src, int n)
{
  int	i;
  int	count;

  i = 0;
  if (n <= 0 )
    return (dest);
  while (i != n)
    {
      dest[i] = src[i];
      i += 1;
    }
  dest[n] = '\0';
  return (dest);
}
