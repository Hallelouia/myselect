/*
** my_strcmp.c for my_strcmp in /home/hallelouia
**
** Made by raphael morand
** Login   <morand_c@epitech.eu>
**
** Started on  Wed Dec 26 14:42:54 2012 raphael morand
** Last update Wed Dec 26 15:18:46 2012 raphael morand
*/

#include "my.h"

int	my_strcmp(char *s1, char *s2)
{
  int	n;

  n = 0;
  while (s1 && s2 && s1[n] == s2[n])
    {
      n++;
      if (my_strlen(s1) - my_strlen(s2) == 0 && n == my_strlen(s1))
	return (0);
    }
  if (s1 && s2 && s1[n] != s2[n])
    return (s1[n] - s2[n]);
  return (0);
}
