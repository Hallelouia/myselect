/*
** my_putchar.c for my_putchar in /afs/epitech.net/users/all/morand_c/rendu/lib/my
**
** Made by raphael morand
** Login   <morand_c@epitech.net>
**
** Started on  Wed Oct 10 10:39:24 2012 raphael morand
** Last update Wed Oct 10 10:40:17 2012 raphael morand
*/

#include "my.h"

void	my_putchar(char c)
{
  write(1, &c , 1);
}
