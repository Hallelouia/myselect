#include "my.h"

int my_power_rec(int nb, int power) {
  if (power == 0)
    return (1);
  if (power < 0 || nb < 0 || nb > 2147483647)
    return (0);
  return (nb * my_power_rec(nb, (power -= 1)));
}
