#include "my.h"

int my_strlcat(char *dest, char *src, int size) {
  int m;
  int n;

  m = 0;
  n = my_strlen(dest);
  while (m != n && n < size) {
    dest[n] = src[m];
    m += 1;
    n += 1;
  }
  return (n);
}
