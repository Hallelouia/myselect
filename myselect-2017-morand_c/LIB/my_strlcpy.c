/*
** my_strlcpy.c for my_strlcpy in /home/hallelouia/SVN/minishell1-2017-morand_c/LIB
** 
** Made by raphael morand
** Login   <morand_c@epitech.eu>
** 
** Started on  Sat Dec 29 15:14:58 2012 raphael morand
** Last update Sat Dec 29 15:18:52 2012 raphael morand
*/

char	*my_strlcpy(char *dest, char *str, int n)
{
  int	a;

  a = 0;
  while (str[n])
    {
      dest[a] = str[n];
      a++;
      n++;
    }
  return (dest);
}
