/*
** my_strncat.c for my_strncat in /home/morand_c//lib
**
** Made by raphael morand
** Login   <morand_c@epitech.net>
**
** Started on  Wed Oct 10 14:38:13 2012 raphael morand
** Last update Fri Nov 16 08:44:28 2012 raphael morand
*/

#include "my.h"

char *my_strncat(char *dest, char *src, int nb) {
  int m;
  int n;

  m = 0;
  n = my_strlen(dest);
  while (m != nb) {
    dest[n] = src[m];
    m += 1;
    n += 1;
  }
  return (dest);
}
