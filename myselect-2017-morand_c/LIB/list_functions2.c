/*
** list_functions2.c for list_functions2 in /home/hallelouia/projets/my_printf
**
** Made by raphael morand
** Login   <morand_c@epitech.eu>
**
** Started on  Thu Nov 15 09:03:29 2012 raphael morand
** Last update Sat Nov 17 17:00:04 2012 raphael morand
*/

#include "my.h"
#include <stdarg.h>

int list_hexamaj(va_list list) {
  unsigned int nb;

  nb = va_arg(list, unsigned int);
  us_putnbr_base(nb, "0123456789ABCDEF");
  return 0;
}

int list_bin(va_list list) {
  unsigned int nb;

  nb = va_arg(list, unsigned int);
  us_putnbr_base(nb, "01");
  return 0;
}

int list_pourcent(va_list list) {
  my_putchar('%');
  return 0;
}

int list_putstr_inv(va_list list) {
  char *str;

  str = va_arg(list, char *);
  putstr_na(str);
  return 0;
}

int list_ptadress(va_list list) {
  void *ptr;

  ptr = va_arg(list, void *);
  my_putstr("0x");
  us_putnbr_base_min((unsigned int)ptr, "0123456789abcdef");
  return 0;
}
