/*
** my_strstr.c for my_strstr in /home/morand_c//afs/rendu/piscine/Jour_06
**
** Made by raphael morand
** Login   <morand_c@epitech.net>
**
** Started on  Mon Oct  8 15:46:34 2012 raphael morand
** Last update Fri Nov 16 08:46:04 2012 raphael morand
*/

#include "my.h"

char *my_strstr(char *str, char *to_find) {
  int i;
  int j;
  int count;

  i = 0;
  j = 0;
  while (str[i] != '\0') {
    while (str[i] == to_find[j]) {
      i += 1;
      j += 1;
    }
    if (str[j] == my_strlen(to_find) - 1)
      return (&str[i - count]);
    i += 1;
  }
  return (0);
}
