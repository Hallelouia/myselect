/*
** my_strncmp.c for my_strncmp in /home/hallelouia
** 
** Made by raphael morand
** Login   <morand_c@epitech.eu>
** 
** Started on  Wed Dec 26 14:51:31 2012 raphael morand
** Last update Wed Dec 26 14:58:50 2012 raphael morand
*/

int     my_strncmp(char *s1, char *s2, int nb)
{
  int   n;

  n = 0;
  while ((s1[n] || s2[n]) && n < nb)
    {
      if (s1[n] != s2[n])
        return (s1[n] - s2[n]);
      n++;
    }
  if ((s1[n] != s2[n]) && n < nb)
    return (s1[n] - s2[n]);
  return (0);
}
