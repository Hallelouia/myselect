/*
** my_putstr_na.c for my_putstr_na in /home/hallelouia/projets/my_printf
**
** Made by raphael morand
** Login   <morand_c@epitech.eu>
**
** Started on  Thu Nov 15 11:13:52 2012 raphael morand
** Last update Thu Nov 15 11:36:33 2012 raphael morand
*/

#include "my.h"

void	putstr_na(char *str)
{
  int	n;

  n = 0;
  while (str[n] != 0)
    {
      if (str[n] < 8)
	{
	  my_putchar('\\');
	  my_putstr("00");
	  my_putnbr_base(str[n], "01234567");
	}
      else if (str[n] < 32)
	{
	  my_putchar('\\');
	  my_putchar('0');
	  my_putnbr_base(str[n], "01234567");
	}
      else if (str[n] == 127)
	my_putnbr_base(str[n], "01234567");
      else
	my_putchar(str[n]);
      n++;
    }
}
