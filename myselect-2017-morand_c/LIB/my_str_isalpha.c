/*
** my_str_isalpha.c for my_str_isalpha in /home/morand_c//piscine/Jour_06/ex_10
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Tue Oct  9 18:13:01 2012 raphael morand
** Last update Tue Oct  9 18:54:46 2012 raphael morand
*/

int	my_str_isalpha(char *str)
{
  int	n;

  n = 0;
  while (str[n] != '\0')
    {
      if (str[n] < 'A' || str[n] > 'z')
	return (0);
      if (str[n] > 'Z' && str[n] < 'a')
	return (0);
      n += 1;
    }
  return (1);
}
