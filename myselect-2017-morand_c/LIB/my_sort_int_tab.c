#include "my.h"

void my_sort_int_tab(int *tab, int size) {
  int n;

  n = 0;
  while (n != size) {
    if (tab[n] > tab[n + 1]) {
      my_swap(&tab[n], &tab[n + 1]);
      n = 0;
    }
    if (tab[n] <= tab[n + 1])
      n += 1;
  }
}
