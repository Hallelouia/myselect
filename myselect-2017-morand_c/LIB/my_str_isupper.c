/*
** my_str_isupper.c for my_str_isupper in /home/morand_c//piscine/Jour_06/ex_13
** 
** Made by raphael morand
** Login   <morand_c@epitech.net>
** 
** Started on  Tue Oct  9 20:43:10 2012 raphael morand
** Last update Tue Oct  9 20:55:52 2012 raphael morand
*/

int	my_str_isupper(char *str)
{
  int	n;

  n = 0;
  while (str[n] != '\0')
    {
      if (str[n] < 65 || str[n] > 90)
	return (0);
      n += 1;
    }
  return (1);
}
