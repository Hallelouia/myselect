/*
** list_functions.c for list_functions in /home/hallelouia/projets/my_printf
**
** Made by raphael morand
** Login   <morand_c@epitech.eu>
**
** Started on  Wed Nov 14 14:01:08 2012 raphael morand
** Last update Thu Nov 15 11:52:46 2012 raphael morand
*/

#include "my.h"
#include <stdarg.h>

int list_putstr(va_list list) {
  char *str;

  str = va_arg(list, char *);
  my_putstr(str);
  return 0;
}

int list_putnbr(va_list list) {
  int nb;

  nb = va_arg(list, int);
  my_putnbr_base(nb, "0123456789");
  return 0;
}

int list_usputnbr(va_list list) {
  unsigned int nb;

  nb = va_arg(list, unsigned int);
  us_putnbr_base(nb, "0123456789");
  return 0;
}

int list_octal(va_list list) {
  unsigned int nb;

  nb = va_arg(list, unsigned int);
  us_putnbr_base(nb, "01234567");
  return 0;
}

int list_hexamin(va_list list) {
  unsigned int nb;

  nb = va_arg(list, unsigned int);
  us_putnbr_base_min(nb, "0123456789abcdef");
  return 0;
}
