#include "my.h"

int my_isneg(int n) {
  if (n < 0) {
    my_putchar('N');
    return 1;
  }
  my_putchar('P');
  return 0;
}
