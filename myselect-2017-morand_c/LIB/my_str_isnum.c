#include "my.h"

int my_str_isnum(char *str) {
  int n;

  n = 0;
  while (str[n] != '\0') {
    if (str[n] < 48 || str[n] > 57)
      return (0);
    n += 1;
  }
  my_putchar('1');
  return (1);
}
