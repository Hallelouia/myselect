/*
** my_strcat.c for my_strcat in /home/hallelouia
**
** Made by raphael morand
** Login   <morand_c@epitech.eu>
**
** Started on  Wed Dec 26 15:03:30 2012 raphael morand
** Last update Sat Dec 29 16:21:36 2012 raphael morand
*/

#include "my.h"

char *my_strcat(char dest[50], char *src) {
  int i;
  int j;

  j = 0;
  i = my_strlen(dest);
  while (src[j])
    dest[i++] = src[j++];
  dest[i] = '\0';
  return (dest);
}
