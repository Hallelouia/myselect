/*
** function.c for func in /home/morand_c//SVN/myselect-2017-morand_c
**
** Made by raphael morand
** Login   <morand_c@epitech.net>
**
** Started on  Thu Jan 17 15:57:52 2013 raphael morand
** Last update Fri Jan 18 13:50:54 2013 raphael morand
*/

#include "dll.h"
#include "my_select.h"

t_dlist *xselect(t_dlist *d_list) {
  t_elem *elem;

  elem = d_list->first;
  while (elem->point != 1)
    elem = elem->next;
  if (elem->select == 0)
    elem->select = 1;
  else if (elem->select == 1)
    elem->select = 0;
  return (d_list);
}

void go_next(t_dlist *d_list) {
  t_elem *elem;

  elem = d_list->first;
  while (elem->point != 1)
    elem = elem->next;
  if (elem->point == 1)
    elem->point = 0;
  elem = elem->next;
  elem->point = 1;
}

void go_prev(t_dlist *d_list) {
  t_elem *elem;

  elem = d_list->last;
  while (elem->point != 1)
    elem = elem->prev;
  if (elem->point == 1)
    elem->point = 0;
  elem = elem->prev;
  elem->point = 1;
}
