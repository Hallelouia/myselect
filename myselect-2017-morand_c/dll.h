/*
** dll.h for dll in /home/hallelouia/liste_double
**
** Made by raphael morand
** Login   <morand_c@epitech.eu>
**
** Started on  Mon Jan 14 13:09:19 2013 raphael morand
** Last update Fri Jan 18 17:58:13 2013 raphael morand
*/

#ifndef DLL_H_
#define DLL_H_

#include <curses.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <term.h>
#include <termcap.h>
#include <termios.h>

#define LIST_EMPTY (0)
#define TOUCH_UP (0x00415B1B)
#define TOUCH_DOWN (0x00425B1B)
#define TOUCH_RIGHT (0x00435B1B)
#define TOUCH_LEFT (0x00445B1B)
#define TOUCH_SPACE (0x00000020)
#define TOUCH_ENTER (0x0000000A)
#define TOUCH_ESC (0x0000001B)
#define TOUCH_DEL (0x7E335B1B)
#define TOUCH_BCKSPACE (0x0000007F)

typedef struct s_elem {
  int select;
  int point;
  void *data;
  struct s_elem *prev;
  struct s_elem *next;
} t_elem;

typedef struct s_dlist {
  int pos;
  size_t size;
  t_elem *first;
  t_elem *last;
} t_dlist;

void go_prev(t_dlist *d_list);
void go_next(t_dlist *d_list);
void circularize(t_dlist *dlist);
void pushback(t_dlist *l, void *val);
void init(t_dlist *l);
t_dlist *dlist_rm_elemn(t_dlist *dlistx);
t_dlist *get_key(int c, t_dlist *d_list);
t_dlist *check(char c, t_dlist *list);
t_dlist *xselect(t_dlist *d_list);
t_dlist *xsuppr(t_dlist *d_list);
t_dlist *xprev(t_dlist *d_list);
t_dlist *xnext(t_dlist *d_list);
t_dlist *dlist_rm_elemn(t_dlist *dlistx);
t_dlist *validate(t_dlist *list);
t_dlist *xnull(t_dlist *dlist);

#endif /* !DLL_H_ */
