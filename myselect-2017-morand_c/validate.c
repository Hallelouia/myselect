/*
** validate.c for my_select in /home/morand_c//SVN/myselect-2017-morand_c
**
** Made by raphael morand
** Login   <morand_c@epitech.net>
**
** Started on  Fri Jan 18 11:25:27 2013 raphael morand
** Last update Fri Jan 18 18:11:34 2013 raphael morand
*/

#include "dll.h"
#include "my_select.h"

t_dlist *validate(t_dlist *list) {
  t_elem *elem;

  elem = list->first;
  list->last->next = NULL;

  if ((tgetent(NULL, "xterm")) == -1)
    exit(EXIT_FAILURE);
  my_putstr(tgetstr("cl", NULL));
  while (elem != NULL) {
    if (elem->select == 1)
      my_printf(" %s", (char *)elem->data);
    elem = elem->next;
  }
  my_putchar('\n');
  exit(EXIT_SUCCESS);
}
