/*
** my_dll_handle.c for dll_handle in /home/morand_c//SVN/myselect-2017-morand_c
**
** Made by raphael morand
** Login   <morand_c@epitech.net>
**
** Started on  Wed Jan 16 11:03:45 2013 raphael morand
** Last update Fri Jan 18 18:12:40 2013 raphael morand
*/

#include "dll.h"
#include "my_select.h"

t_dlist *del(t_dlist *dlist, t_elem *elem) {
  if (elem == dlist->last) {
    dlist->last = elem->prev;
    elem->prev->next = dlist->first;
    dlist->first->prev = elem->prev;
  } else if (elem == dlist->first) {
    dlist->first = elem->next;
    elem->next->prev = dlist->last;
    dlist->last->next = elem->next;
  } else {
    elem->next->prev = elem->prev;
    elem->prev->next = elem->next;
  }
  free(elem);
  return dlist;
}

t_dlist *dlist_rm_elemn(t_dlist *dlist) {
  int n;
  t_elem *elem;

  elem = dlist->first;
  n = 1;
  if (dlist->size <= 1) {
    if ((tgetent(NULL, "xterm")) == -1)
      exit(EXIT_FAILURE);
    free(elem);
    my_putstr(tgetstr("cl", NULL));
    exit(LIST_EMPTY);
  }
  while (elem != NULL && n <= (dlist->pos)) {
    if (n == (dlist->pos))
      dlist = del(dlist, elem);
    else
      elem = elem->next;
    n++;
  }
  return dlist;
}
